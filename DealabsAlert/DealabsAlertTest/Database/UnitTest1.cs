﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;


using DealabsDatabase;
using System.Data.SQLite;
using System.Data;
using System.IO;


namespace DealabsAlertTest.Database
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Database1()
        {
            if(File.Exists("MyDatabase.sqlite") == false)
                SQLiteConnection.CreateFile("MyDatabase.sqlite");

            SQLiteConnection db = new SQLiteConnection("Data Source=MyDatabase.sqlite;Version=3;");
            Assert.AreEqual(ConnectionState.Closed, db.State);
            db.Open();

            Assert.AreEqual(ConnectionState.Open, db.State);

            SQLiteCommand Command = new SQLiteCommand("create table if not exists test (name varchar(20), score varchar(255));", db);

            Command.ExecuteNonQuery();

            Command = new SQLiteCommand("insert into test values (0, 'test');", db);
            Command.ExecuteNonQuery();
            Command = new SQLiteCommand("insert into test values (0, 'test')", db);
            Command.ExecuteNonQuery();
            Command = new SQLiteCommand("insert into test values (0, 'test')", db);
            Command.ExecuteNonQuery();
            Command = new SQLiteCommand("insert into test values (0, 'test')", db);
            Command.ExecuteNonQuery();
            Command = new SQLiteCommand("insert into test values (0, 'test')", db);
            Command.ExecuteNonQuery();

            Command = new SQLiteCommand("select * from test", db);
            Command.Parameters.Add(new SQLiteParameter("test", "value"));
            SQLiteDataReader Reader = Command.ExecuteReader();

            
        }
    }
}
